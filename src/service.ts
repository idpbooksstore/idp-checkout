import { CheckoutData } from './types';
import {
  verifyTokenRequest,
  getItemsRequest,
  getBooksRequest,
  makePaymentRequest,
  makeShippingRequest,
  clearItemsRequest,
} from './request';

export const makeCheckout = async (token: string, address: string, card: CheckoutData) => {
  const tokenResponse = await verifyTokenRequest(token);
  if (tokenResponse.status !== 200) return { status: tokenResponse.status, message: tokenResponse.message };

  const itemsResponse = await getItemsRequest(tokenResponse.user);
  if (itemsResponse.status !== 200) return { status: itemsResponse.status, message: itemsResponse.message };
  if (itemsResponse.items === []) return { status: 400, message: 'Cannot checkout empty cart' };

  const booksResponse = await getBooksRequest(itemsResponse.items);
  if (booksResponse.status !== 200) return { status: booksResponse.status, message: booksResponse.message };

  const prices: NodeJS.Dict<number> = {};
  booksResponse.books.forEach((book) => (prices[book._id] = book.price));
  const price = itemsResponse.items.map((item) => prices[item._id]! * item.amount).reduce((prev, curr) => prev + curr);

  const paymentResponse = await makePaymentRequest(price, card);
  if (paymentResponse.status !== 200) return { status: paymentResponse.status, message: paymentResponse.message };

  const shippingResponse = await makeShippingRequest(itemsResponse.items, address);
  if (shippingResponse.status !== 200) return { status: shippingResponse.status, message: shippingResponse.message };

  const clearResponse = await clearItemsRequest(tokenResponse.user);
  if (clearResponse.status !== 200) return { status: clearResponse.status, message: clearResponse.message };

  return { status: 200, message: 'Success' };
};
