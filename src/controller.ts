import { makeCheckout } from './service';
import Router, { Request, Response } from 'express';
import { validateCheckout } from './validate';
import { CheckoutData } from './types';
import { env } from './env';

export const router = Router();

interface PostCheckout {
  address: string;
  card: CheckoutData;
}

export const postCheckout = async (req: Request, res: Response) => {
  if (!validateCheckout(req.body)) return res.status(400).send({ message: validateCheckout.errors });
  try {
    const token = req.header(env.auth.token);
    if (token === undefined) return res.status(401).send({ message: 'Undefined Token' });

    const { address, card } = req.body as PostCheckout;
    const { status, message } = await makeCheckout(token, address, card);

    res.status(status).send({ message });
  } catch (error) {
    return res.status(500).send({ message: error });
  }
};

router.post('/', postCheckout);
