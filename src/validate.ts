import Ajv from 'ajv';

const ajv = new Ajv();

const checkoutSchema = {
  type: 'object',
  properties: {
    address: { type: 'string' },
    card: {
      type: 'object',
      properties: {
        card: { type: 'string' },
        cvc: { type: 'string' },
        date: { type: 'string' },
      },
    },
  },
  additionalProperties: false,
};

export const validateCheckout = ajv.compile(checkoutSchema);
