import axios, { AxiosRequestConfig, AxiosPromise } from 'axios';
import { CheckoutData, ItemData, BookData } from './types';
import { env } from './env';

interface ItemsResponse {
  message: string;
  items: ItemData[];
}

interface GetItemsRequest {
  status: number;
  message: string;
  items: ItemData[];
}

interface ClearItemsRequest {
  status: number;
  message: string;
  items: ItemData[];
}

interface SessionResponse {
  message: string;
  user: string;
}

interface VerifyTokenRequest {
  status: number;
  message: string;
  user: string;
}

interface BookResponse {
  message: string;
  books: BookData[];
}

interface GetBooksRequest {
  status: number;
  message: string;
  books: BookData[];
}

interface MakePaymentRequest {
  status: number;
  message: string;
}

interface PaymentResponse {
  message: string;
}

interface MakeShippingRequest {
  status: number;
  message: string;
}

interface ShippingResponse {
  message: string;
}

export const sendRequest = async <T>(options: AxiosRequestConfig): Promise<AxiosPromise<T>> => axios(options);

export const verifyTokenRequest = async (token: string): Promise<VerifyTokenRequest> => {
  try {
    const response = await sendRequest<SessionResponse>({
      url: `http://${env.session.host}:${env.session.port}/api/session/verify/${token}`,
      method: 'GET',
    });
    return { status: response.status, message: response.data.message, user: response.data.user };
  } catch (error) {
    return { status: 500, message: error, user: '' };
  }
};

export const getItemsRequest = async (user: string): Promise<GetItemsRequest> => {
  try {
    const response = await sendRequest<ItemsResponse>({
      url: `http://${env.cartStore.host}:${env.cartStore.port}/api/cart/store/${user}`,
      method: 'GET',
    });
    return { status: response.status, message: response.data.message, items: response.data.items };
  } catch (error) {
    return { status: 500, message: error, items: [] };
  }
};

export const clearItemsRequest = async (user: string): Promise<ClearItemsRequest> => {
  try {
    const response = await sendRequest<ItemsResponse>({
      url: `http://${env.cartStore.host}:${env.cartStore.port}/api/cart/store/${user}`,
      method: 'DELETE',
    });
    return { status: response.status, message: response.data.message, items: response.data.items };
  } catch (error) {
    return { status: 500, message: error, items: [] };
  }
};

export const getBooksRequest = async (items: ItemData[]): Promise<GetBooksRequest> => {
  try {
    const ids = items.map((item) => `id[]=${item._id}`).join('&');
    const response = await sendRequest<BookResponse>({
      url: `http://${env.books.host}:${env.books.port}/api/books?${ids}`,
      method: 'GET',
    });
    return { status: response.status, message: response.data.message, books: response.data.books };
  } catch (error) {
    return { status: 500, message: error, books: [] };
  }
};

export const makePaymentRequest = async (price: number, card: CheckoutData): Promise<MakePaymentRequest> => {
  try {
    const response = await sendRequest<PaymentResponse>({
      url: `http://${env.payment.host}:${env.payment.port}/api/payment`,
      method: 'POST',
      data: {
        card,
        amount: price,
      },
    });
    return { status: response.status, message: response.data.message };
  } catch (error) {
    return { status: 500, message: error };
  }
};

export const makeShippingRequest = async (items: ItemData[], address: string): Promise<MakeShippingRequest> => {
  try {
    const response = await sendRequest<ShippingResponse>({
      url: `http://${env.shipping.host}:${env.shipping.port}/api/shipping`,
      method: 'POST',
      data: {
        items,
        address,
      },
    });
    return { status: response.status, message: response.data.message };
  } catch (error) {
    return { status: 500, message: error };
  }
};
