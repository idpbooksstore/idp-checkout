import { router } from './controller';
import express from 'express';
import { env } from './env';

const PORT = env.server.port;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api/checkout', router);

app.listen(PORT);
