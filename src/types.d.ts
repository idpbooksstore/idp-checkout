export interface CheckoutData {
  card: string;
  cvc: string;
  date: string;
}

export interface ItemData {
  _id: string;
  amount: number;
}

export interface BookData {
  _id: string;
  title: string;
  author: string;
  genre: string;
  reviews: ReviewData[];
  price: number;
}
