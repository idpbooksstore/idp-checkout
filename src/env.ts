import dotenv from 'dotenv';
import fs from 'fs';

dotenv.config();

export const env = {
  server: {
    port: +(process.env.PORT ?? 8080),
  },
  books: {
    host: process.env.BOOKS_HOST ?? 'localhost',
    port: +(process.env.BOOKS_PORT ?? 8080),
  },
  cartStore: {
    host: process.env.CART_STORE_HOST ?? 'localhost',
    port: +(process.env.CART_STORE_PORT ?? 8080),
  },
  session: {
    host: process.env.SESSION_HOST ?? 'localhost',
    port: +(process.env.SESSION_PORT ?? 8080),
  },
  payment: {
    host: process.env.PAYMENT_HOST ?? 'localhost',
    port: +(process.env.PAYMENT_PORT ?? 8080),
  },
  shipping: {
    host: process.env.SHIPPING_HOST ?? 'localhost',
    port: +(process.env.SHIPPING_PORT ?? 8080),
  },

  auth: {
    token: process.env.AUTH_TOKEN ?? 'authtoken',
    token_file: process.env.AUTH_TOKEN_FILE,
  },
  nodeEnv: process.env.NODE_ENV ?? 'development',
};

if (env.auth.token_file) {
  env.auth.token = fs.readFileSync(env.auth.token_file, 'utf8');
}
